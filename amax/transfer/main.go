package main

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/armoniax/eos-go"
	"github.com/armoniax/eos-go/token"
)

const (
	//api = "https://test-chain.ambt.art"
	api = "http://testnet.amax.dev:18888"
	//api = "http://sg-exp2.vm.nchain.me:18888"
	//api           = "http://t1.cnyg.io:18888"
	myAccountName = "amaxtesta1a1"
	myPrivateKey  = "5JdRa6fREBxVhZWjVtJgWEp4wZN2PyUu6kwDL64DvaVkygCBCGj"
	myPermission  = "active"

	toAccountName = "sandbox11111"
)

func main() {
	client := eos.New(api)
	ctx := context.Background()

	keyBag := &eos.KeyBag{}
	err := keyBag.ImportAmaxPrivateKey(ctx, myPrivateKey)
	if err != nil {
		panic(fmt.Errorf("import private key: %w", err))
	}
	client.SetSigner(keyBag)
	client.Debug = true

	from := eos.AccountName(myAccountName)
	to := eos.AccountName(toAccountName)

	symbol := eos.Symbol{
		Precision: 8,
		Symbol:    "AMAX",
	}

	amounts := fmt.Sprintf("%v AMAX", 0.001)
	quantity, err := eos.NewFixedSymbolAssetFromString(symbol, amounts)
	memo := "test transfer push call debug"

	fmt.Printf("quantity: %#v\n", quantity)

	if err != nil {
		panic(fmt.Errorf("invalid quantity: %w", err))
	}

	txOpts := &eos.TxOptions{}
	if err := txOpts.FillFromChain(ctx, client); err != nil {
		panic(fmt.Errorf("filling tx opts: %w", err))
	}

	// 其他action自己定义，类似 NewTransfer
	// 也可以直接使用 SignPushActions 方法，sign & push
	//tx := eos.NewTransaction([]*eos.Action{actions.NewAmaxTransfer(from, to, "amax.token", quantity, memo)}, txOpts)
	tx := eos.NewTransaction([]*eos.Action{token.NewTransfer(from, to, quantity, memo)}, txOpts)

	signedTrx, packedTrx, err := client.SignTransaction(ctx, tx, txOpts.ChainID, eos.CompressionNone)
	if err != nil {
		panic(fmt.Errorf("sign transaction: %w", err))
	}

	fmt.Printf("sign-mesg degist: %v\n", signedTrx.Actions[0].Digest())

	content, err := json.MarshalIndent(signedTrx, "", "  ")
	if err != nil {
		panic(fmt.Errorf("json marshalling transaction: %w", err))
	}

	fmt.Printf("signedTrx: %s\n\n", string(content))
	fmt.Printf("packedTrx: %s\n\n", packedTrx.PackedTransaction.String())

	response, err := client.PushTransaction(ctx, packedTrx)
	if err != nil {
		panic(fmt.Errorf("push transaction: %w", err))
	}

	fmt.Printf("amax-push transaction, response====: %v\n", response)

	fmt.Printf("Transaction [%s] submitted to the network succesfully.\n", hex.EncodeToString(response.Processed.ID))
}
