package main

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"eos-go-demo/actions"
	"fmt"
	"github.com/armoniax/eos-go"
)

const (
	api = "https://test-chain.ambt.art"
	//api           = "http://t1.cnyg.io:18888"
	myAccountName = "msigtesta111"
	myPrivateKey  = "5JLG1H4gmRFUSJVR6W6jXmnK9G12GasyG1NMj5ihmHewbp3hZhn"
)

func main() {
	client := eos.New(api)
	ctx := context.Background()

	keyBag := &eos.KeyBag{}
	err := keyBag.ImportAmaxPrivateKey(ctx, myPrivateKey)
	if err != nil {
		panic(fmt.Errorf("import private key: %w", err))
	}
	client.SetSigner(keyBag)
	client.Debug = true

	txOpts := &eos.TxOptions{}
	if err := txOpts.FillFromChain(ctx, client); err != nil {
		panic(fmt.Errorf("filling tx opts: %w", err))
	}

	account := eos.AN("msigtesta111")
	permission := eos.PN("owner")
	parent := eos.PN("")
	usingPermission := eos.PN("owner")

	var keys []eos.KeyWeight
	var accounts []eos.PermissionLevelWeight
	var waits []eos.WaitWeight

	accounts = append(accounts, eos.PermissionLevelWeight{
		Permission: eos.PermissionLevel{
			Actor:      eos.AN("msigtest1155"),
			Permission: "active",
		},
		Weight: 1,
	}, eos.PermissionLevelWeight{
		Permission: eos.PermissionLevel{
			Actor:      eos.AN("msigtesta222"),
			Permission: "active",
		},
		Weight: 1,
	})

	quthority := eos.Authority{
		Threshold: 2,
		Keys:      keys,
		Accounts:  accounts,
		Waits:     waits,
	}

	updateAuthAction := actions.NewUpdateAuth(account, permission, parent, quthority, usingPermission)

	tx := eos.NewTransaction([]*eos.Action{updateAuthAction}, txOpts)

	signedTrx, packedTrx, err := client.SignTransaction(ctx, tx, txOpts.ChainID, eos.CompressionNone)
	if err != nil {
		panic(fmt.Errorf("sign transaction: %w", err))
	}

	//fmt.Printf("sign-mesg degist: %v\n", signedTrx.Actions[0].Digest())

	content, err := json.MarshalIndent(signedTrx, "", "  ")
	if err != nil {
		panic(fmt.Errorf("json marshalling transaction: %w", err))
	}

	fmt.Printf("signedTrx: %s\n\n", string(content))
	fmt.Printf("packedTrx: %s\n\n", packedTrx.PackedTransaction.String())

	response, err := client.PushTransaction(ctx, packedTrx)
	if err != nil {
		panic(fmt.Errorf("push transaction: %w", err))
	}

	fmt.Printf("amax-push transaction, response====: %v\n", response)

	fmt.Printf("Transaction [%s] submitted to the network succesfully.\n", hex.EncodeToString(response.Processed.ID))
}
