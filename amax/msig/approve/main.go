package main

import (
	"context"
	"eos-go-demo/internal/chain/amax"
	"eos-go-demo/internal/core/log"
	"fmt"
	"github.com/armoniax/eos-go"
)

const (
	rpc = "https://test-chain.ambt.art"
	//api           = "http://t1.cnyg.io:18888"
	msigAccount  = "msigtestc4c4"
	myPrivateKey = "5JiXFrFRdPMz8bMcrajeaUUG8iLcKrPfMyJzJ7HLALgnwGD4mcC"
	myPermission = "active"

	toAccountName = "sandbox11111"
)

func main() {
	api := eos.New(rpc)
	ctx := context.Background()

	keyBag := &eos.KeyBag{}
	err := keyBag.ImportAmaxPrivateKey(ctx, myPrivateKey)
	if err != nil {
		panic(fmt.Errorf("import private key: %w", err))
	}
	api.SetSigner(keyBag)
	api.Debug = true

	txhash, err := amax.PushProposeApproveTransaction(ctx, api, msigAccount, "msigtestc2c2", "multisigczwq")
	if err != nil {
		log.Logger.Errorf("error pushing proposer transaction err: %s", err.Error())
		return
	}

	log.Logger.Infof("push ProposeApprove transaction tx_hash: %s", txhash)

}
