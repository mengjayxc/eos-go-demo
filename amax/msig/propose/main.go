package main

import (
	"context"
	"eos-go-demo/internal/chain/amax"
	"eos-go-demo/internal/core/log"
	"fmt"
	"github.com/armoniax/eos-go"
)

const (
	rpc = "https://test-chain.ambt.art"
	//api           = "http://t1.cnyg.io:18888"
	myAccountName = "msigtestc2c2"
	myPrivateKey  = "5JmzqYYN7qPxZsFxg3nnWxJhaeJMvLzSqcyMnXNHDFTtgAcDyeb"
	myPermission  = "active"

	toAccountName = "sandbox11111"
)

func main() {
	api := eos.New(rpc)
	ctx := context.Background()

	keyBag := &eos.KeyBag{}
	err := keyBag.ImportAmaxPrivateKey(ctx, myPrivateKey)
	if err != nil {
		panic(fmt.Errorf("import private key: %w", err))
	}
	api.SetSigner(keyBag)
	api.Debug = true

	actors := []string{"msigtestc2c2", "msigtestc4c4"}

	txhash, err := amax.PushProposeTransaction(ctx, api, myAccountName, "multisigczwq", actors, "msigtestc1c1", toAccountName, "amax.token", "AMAX", "test transfer push call of update auth", 100000000, 8)
	if err != nil {
		log.Logger.Errorf("error pushing proposer transaction err: %s", err.Error())
		return
	}

	log.Logger.Infof("push transaction tx_hash: %s", txhash)

}
