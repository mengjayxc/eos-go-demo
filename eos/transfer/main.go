package main

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/armoniax/eos-go"
	"github.com/armoniax/eos-go/token"
)

const (
	api           = "https://jungle3.cryptolions.io"
	myAccountName = "eosisverygo1"
	myPublicKey   = "EOS6aZPsm846vGvf96gRpEoKUYadvaC53i6RQHchuQxn9Dy8FqWPH"
	myPrivateKey  = "5Kfs388vr6TM8oMsCd4xfkqsR9132nw9TWfpma7NNfRzvMN3vxA"
	myPermission  = "active"

	toAccountName = "eosisverygo2"
)

func main() {
	client := eos.New(api)
	ctx := context.Background()

	keyBag := &eos.KeyBag{}
	err := keyBag.ImportPrivateKey(ctx, myPrivateKey)
	if err != nil {
		panic(fmt.Errorf("import private key: %w", err))
	}
	client.SetSigner(keyBag)

	from := eos.AccountName(myAccountName)
	to := eos.AccountName(toAccountName)
	//quantity, err := eos.NewEOSAssetFromString("1.0000 EOS")
	quantity, err := eos.NewAssetFromString("1.0000 JUNGLE")
	memo := "test transfer"

	fmt.Printf("quantity: %#v\n", quantity)
	//  eos.Asset{Amount:10000, Symbol:eos.Symbol{Precision:0x4, Symbol:"JUNGLE", symbolCode:0x0}}
	// quantity: Failed to compute node presentation

	if err != nil {
		panic(fmt.Errorf("invalid quantity: %w", err))
	}

	txOpts := &eos.TxOptions{}
	if err := txOpts.FillFromChain(context.Background(), client); err != nil {
		panic(fmt.Errorf("filling tx opts: %w", err))
	}

	// 其他action自己定义，类似 NewTransfer
	// 也可以直接使用 SignPushActions 方法，sign & push
	tx := eos.NewTransaction([]*eos.Action{token.NewTransfer(from, to, quantity, memo)}, txOpts)
	signedTx, packedTx, err := client.SignTransaction(context.Background(), tx, txOpts.ChainID, eos.CompressionNone)
	if err != nil {
		panic(fmt.Errorf("sign transaction: %w", err))
	}

	content, err := json.MarshalIndent(signedTx, "", "  ")
	if err != nil {
		panic(fmt.Errorf("json marshalling transaction: %w", err))
	}

	fmt.Printf("signedTx: %s\n\n", string(content))

	response, err := client.PushTransaction(context.Background(), packedTx)
	if err != nil {
		panic(fmt.Errorf("push transaction: %w", err))
	}

	fmt.Printf("Transaction [%s] submitted to the network succesfully.\n", hex.EncodeToString(response.Processed.ID))
}
