package main

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"eos-go-demo/actions"
	"fmt"
	"github.com/armoniax/eos-go"
)

const (
	api           = "https://jungle3.cryptolions.io"
	myAccountName = "eosisverygo1"
	myPublicKey   = "EOS6aZPsm846vGvf96gRpEoKUYadvaC53i6RQHchuQxn9Dy8FqWPH"
	myPrivateKey  = "5Kfs388vr6TM8oMsCd4xfkqsR9132nw9TWfpma7NNfRzvMN3vxA"
	myPermission  = "active"
)

func main() {
	client := eos.New(api)
	ctx := context.Background()

	keyBag := &eos.KeyBag{}
	err := keyBag.ImportPrivateKey(ctx, myPrivateKey)
	if err != nil {
		panic(fmt.Errorf("import private key: %w", err))
	}
	client.SetSigner(keyBag)

	// 可以填写抵押资源借给对方的账号（回收）
	from := eos.AccountName(myAccountName)
	receiver := eos.AccountName(myAccountName)

	unStakeNetQuantity, err := eos.NewAssetFromString("0.5000 EOS")
	if err != nil {
		panic(fmt.Errorf("invalid unStakeNetQuantity: %w", err))
	}
	unStakeCpuQuantity, err := eos.NewAssetFromString("1.0000 EOS")
	if err != nil {
		panic(fmt.Errorf("invalid unStakeCpuQuantity: %w", err))
	}

	txOpts := &eos.TxOptions{}
	if err := txOpts.FillFromChain(context.Background(), client); err != nil {
		panic(fmt.Errorf("filling tx opts: %w", err))
	}

	tx := eos.NewTransaction([]*eos.Action{actions.NewUnstake(from, receiver, "eosio", unStakeNetQuantity, unStakeCpuQuantity)}, txOpts)
	signedTx, packedTx, err := client.SignTransaction(context.Background(), tx, txOpts.ChainID, eos.CompressionNone)
	if err != nil {
		panic(fmt.Errorf("sign transaction: %w", err))
	}

	content, err := json.MarshalIndent(signedTx, "", "  ")
	if err != nil {
		panic(fmt.Errorf("json marshalling transaction: %w", err))
	}

	fmt.Printf("signedTx: %s\n\n", string(content))

	response, err := client.PushTransaction(context.Background(), packedTx)
	fmt.Printf("res: %v\n", response)
	if err != nil {
		panic(fmt.Errorf("push transaction: %w", err))
	}

	fmt.Printf("Transaction [%s] submitted to the network succesfully.\n", hex.EncodeToString(response.Processed.ID))
}
