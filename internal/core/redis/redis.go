package redis

import (
	"eos-go-demo/internal/core/log"
	"fmt"

	"github.com/go-redis/redis"
)

type redisHelper struct {
	Client *redis.Client
}

// RedisDB redisdb instance
var RedisDB *redisHelper

// InitRedisDB initial redisDB
func Init(host, port, password string, db, poolSize, minIdle int) {
	client := redis.NewClient(&redis.Options{
		Addr:         fmt.Sprintf("%s:%s", host, port),
		MaxRetries:   30,
		DB:           db,
		Password:     password,
		PoolSize:     poolSize,
		MinIdleConns: minIdle,
	})

	RedisDB = &redisHelper{Client: client}
	if err := RedisDB.Client.Ping().Err(); err != nil {
		log.Logger.Errorf("redis init failed: %s", err.Error())
		panic(any(err))
	} else {
		log.Logger.Info("redis init success")
	}

}
