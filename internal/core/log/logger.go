package log

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var Logger *zap.SugaredLogger
var ZapLogger *zap.Logger
var ErrorLogger *zap.SugaredLogger

func init() {
	//default init
	var log *zap.Logger
	var zapLevel zapcore.Level

	encoderConfig := zapcore.EncoderConfig{
		TimeKey:        "timestamp",
		LevelKey:       "severity",
		NameKey:        "logger",
		CallerKey:      "caller",
		MessageKey:     "message",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.ISO8601TimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.FullCallerEncoder,
	}

	if level, ok := zapLogLevelMapping["info"]; ok {
		zapLevel = level
	} else {
		zapLevel = zapcore.InfoLevel
	}

	atom := zap.NewAtomicLevelAt(zapLevel)

	config := zap.Config{
		Level:            atom,
		Development:      true,
		Encoding:         "json",
		EncoderConfig:    encoderConfig,
		InitialFields:    map[string]interface{}{"serviceName": ""},
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stdout"},
	}

	log, _ = config.Build()
	Logger = log.Sugar()
	ZapLogger = log
}

func Init(level, serviceName string) error {
	var err error
	var log *zap.Logger
	var zapLevel zapcore.Level

	encoderConfig := zapcore.EncoderConfig{
		TimeKey:        "timestamp",
		LevelKey:       "severity",
		NameKey:        "logger",
		CallerKey:      "caller",
		MessageKey:     "message",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.ISO8601TimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.FullCallerEncoder,
	}

	if level, ok := zapLogLevelMapping[level]; ok {
		zapLevel = level
	} else {
		zapLevel = zapcore.InfoLevel
	}

	atom := zap.NewAtomicLevelAt(zapLevel)

	config := zap.Config{
		Level:            atom,
		Development:      true,
		Encoding:         "json",
		EncoderConfig:    encoderConfig,
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stdout"},
	}

	log, err = config.Build()
	if err != nil {
		return err
	}
	Logger = log.Sugar()
	ZapLogger = log

	config.InitialFields = map[string]interface{}{
		"@type":       "type.googleapis.com/google.devtools.clouderrorreporting.v1beta1.ReportedErrorEvent",
		"serviceName": serviceName,
		"serviceContext": map[string]string{
			"service": serviceName,
			"version": level,
		},
	}
	log2, err := config.Build()
	if err != nil {
		return err
	}
	ErrorLogger = log2.Sugar()
	return nil
}

func Flush() {
	Logger.Sync()
	ErrorLogger.Sync()
}

var zapLogLevelMapping = map[string]zapcore.Level{
	"debug": zapcore.DebugLevel,
	"info":  zapcore.InfoLevel,
	"warn":  zapcore.WarnLevel,
	"error": zapcore.ErrorLevel,
	"panic": zapcore.PanicLevel,
	"fatal": zapcore.FatalLevel,
}
