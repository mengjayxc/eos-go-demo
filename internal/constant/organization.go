package constant

const (
	// 余额变动类型
	BalanceTypeOnlyBalance         = 1 // 仅变动(加/减)可用余额
	BalanceTypeOnlyFrozenBalance   = 2 // 仅变动冻结余额
	BalanceTypeSubBalanceAddFrozen = 3 // 减可用加冻结
	BalanceTypeAddBalanceSubFrozen = 4 // 加可用减冻结

	// 余额流水类型
	BalanceFlowTypeDepositConfirming = 257 // 充值确认中
	BalanceFlowTypeDeposit           = 258 // 充值到账
	BalanceFlowTypeWithdrawFrozen    = 513 // 提现冻结
	BalanceFlowTypeWithdraw          = 514 // 提现成功扣款
	BalanceFlowTypeWithdrawUnfrozen  = 515 // 提现解冻
	BalanceFlowTypeAdjustment        = 769 // 调账

	// 提现请求状态
	WithdrawToBeReviewed              = 1  // 待审核
	WithdrawReviewRejected            = 2  // 审核拒绝
	WithdrawSuccessRefund             = 3  // 退款成功
	WithdrawFailedRefund              = 4  // 退款失败
	WithdrawReviewedSucceedToProccees = 5  // 审核成功待处理
	WithdrawTransactionToProccees     = 6  // 交易构建中
	WithdrawPushTrxFailed             = 7  // 交易构建失败
	WithdrawPushTrxSucceedConfirming  = 8  // 交易构建成功确认中
	WithdrawTransactionFailed         = 9  // 交易失败
	WithdrawTransactionSucceed        = 10 // 交易成功
)
