package constant

const (
	// 交易方向
	TranSideDeposit  = "deposit"  // 充值
	TranSideWithdraw = "withdraw" // 提现

	// 交易状态
	TranStatusSuccess = "success" // 成功
	TranStatusFailed  = "failed"  // 失败
	TranStatusPending = "pending" // 打包中

	TranTypeExternal = "external" // 上链交易
	TranTypeInternal = "internal" // loop交易

	SignTypeLocal = "local" // 本地私钥签名
	SignTypeKms   = "kms"   // kms签名

	ProposalNamePrefixGather = "g." // 归集交易提案前缀，后面为org transaction表id ex:g.1, g.123
)
