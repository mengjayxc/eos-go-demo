package constant

const (
	// 主链币种
	ChainAmax = "AMAX"
	ChainBtc  = "BTC"
	ChainEth  = "ETH"
	ChainTrx  = "TRX"
	ChainBsc  = "BSC"

	TokenFormat = "%v_%v" // 代币格式:左主链币简称 右代币简称, ex:AMAX_CNYG
)
