package constant

import "github.com/armoniax/eos-go"

const (
	LanguageZh = "zh-cn"
	LanguageEn = "en-us"
)

const (
	ContextApiKey = "api-key"
	ContextOrg    = "org" // 组织模型信息

	// 头部签名参数
	HeaderApiKey        = "Biz-Api-Key"
	HeaderApiSignature  = "Biz-Api-Signature"
	HeaderApiNonce      = "Biz-Api-Nonce"
	HeaderTimestamp     = "Biz-Timestamp"
	HeaderRespSignature = "Biz-Resp-Signature"
)

var AbiString = `
{
    "version": "amax::abi/1.1", 
    "types": [
        {
            "new_type_name": "chain_alias", 
            "type": "uint8"
        }, 
        {
            "new_type_name": "chain_id", 
            "type": "checksum256"
        }, 
        {
            "new_type_name": "request_flags", 
            "type": "uint8"
        }
    ], 
    "structs": [
        {
            "base": "", 
            "name": "permission_level", 
            "fields": [
                {
                    "name": "actor", 
                    "type": "name"
                }, 
                {
                    "name": "permission", 
                    "type": "name"
                }
            ]
        }, 
        {
            "base": "", 
            "name": "action", 
            "fields": [
                {
                    "name": "account", 
                    "type": "name"
                }, 
                {
                    "name": "name", 
                    "type": "name"
                }, 
                {
                    "name": "authorization", 
                    "type": "permission_level[]"
                }, 
                {
                    "name": "data", 
                    "type": "identity"
                }
            ]
        }, 
        {
            "base": "", 
            "name": "identity", 
            "fields": [
                {
                    "name": "scope", 
                    "type": "name"
                }, 
                {
                    "name": "permission", 
                    "type": "permission_level?"
                }
            ]
        }
    ], 
    "actions": [ ], 
    "tables": [ ], 
    "ricardian_clauses": [ ], 
    "error_messages": [ ], 
    "abi_extensions": [ ], 
    "variants": [
        {
            "name": "variant_id", 
            "types": [
                "chain_alias", 
                "chain_id"
            ]
        }, 
        {
            "name": "variant_req", 
            "types": [
                "action", 
                "action[]", 
                "transaction", 
                "identity"
            ]
        }
    ]
}
`

type Identity struct {
	Scope      eos.Name            `json:"scope"`
	Permission eos.PermissionLevel `json:"permission"`
}
