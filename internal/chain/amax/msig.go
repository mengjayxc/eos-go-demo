package amax

import (
	"context"
	"eos-go-demo/internal/chain/amax/amaxmsig"
	"eos-go-demo/internal/core/log"
	"errors"
	"regexp"
	"time"

	"github.com/gogf/gf/v2/util/gconv"

	"github.com/armoniax/eos-go"
)

// PushProposeTransaction 发送新提案交易
func PushProposeTransaction(ctx context.Context, api *eos.API, proposer, proposalName string, actors []string,
	sourceAddress, toAddress, tokenAddress, coinSymbol, memo string, amount uint64, coinDecimal uint8) (string, error) {

	if len(actors) <= 0 {
		return "", errors.New("actors cannot be empty")
	}

	txOpts := &eos.TxOptions{}
	if err := txOpts.FillFromChain(ctx, api); err != nil {
		log.Logger.Errorf("Failed to fill tx err: %s", err.Error())
		return "", err
	}

	// 原始转账交易
	rawTx, err := NewTransaction(api, ctx, txOpts, sourceAddress, toAddress, tokenAddress, coinSymbol, memo, amount, coinDecimal)
	if err != nil {
		log.Logger.Errorf("amax NewTransaction err: %s", err.Error())
		return "", err
	}
	// 设置过期时间
	rawTx.SetExpiration(24 * time.Hour)

	// 提案交易
	tx, err := NewProposeTx(ctx, api, txOpts, proposer, proposalName, actors, rawTx)
	if err != nil {
		log.Logger.Errorf("NewProposeTx err: %s", err.Error())
		return "", err
	}

	txHash, err := SignAndPush(ctx, api, txOpts, tx)
	if err != nil {
		log.Logger.Errorf("propose push transaction err: %s", err.Error())
		return "", err
	}

	return txHash, nil
}

func NewProposeTx(ctx context.Context, api *eos.API, txOpts *eos.TxOptions, proposer, proposalName string, actors []string, rawTx *eos.Transaction) (*eos.Transaction, error) {
	if txOpts == nil {
		txOpts = &eos.TxOptions{}
		if err := txOpts.FillFromChain(ctx, api); err != nil {
			log.Logger.Errorf("Failed to fill tx err: %s", err.Error())
			return nil, err
		}
	}

	if proposalName == "" {
		// 默认提案名使用自增数字
		proposalName = "1"

		// 查最新一笔提案
		resp, err := api.GetTableRows(ctx, eos.GetTableRowsRequest{
			Code:    "amax.msig",
			Scope:   proposer,
			Table:   "approvals2",
			Limit:   1,
			Reverse: true,
			JSON:    true,
		})
		if err != nil {
			log.Logger.Warnf("api.GetTableRows err:%v", err.Error())
			return nil, err
		}

		var rows []*amaxmsig.Approvals2Row
		err = resp.JSONToStructs(&rows)
		if err != nil {
			log.Logger.Warnf("GetTableRows resp.JSONToStructs err:%v", err.Error())
			return nil, err
		}

		if len(rows) > 0 {
			oldProposalName := rows[0].ProposalName.String()
			re := regexp.MustCompile(`(\d+)$`) // 匹配字符串末尾的数字
			matches := re.FindAllString(oldProposalName, -1)
			if len(matches) <= 0 {
				proposalName = oldProposalName + gconv.String(1)
			} else {
				lastMatch := matches[len(matches)-1]
				proposalName = re.ReplaceAllString(oldProposalName, gconv.String(gconv.Int64(lastMatch)+1)) // 末尾数字自增
			}
		}
	}

	// 添加哪些账户和对应权限要签名
	var requested []eos.PermissionLevel
	for _, actor := range actors {
		r := eos.PermissionLevel{
			Actor:      eos.AccountName(actor),
			Permission: eos.PermissionName("active"),
		}
		requested = append(requested, r)
	}

	proposeAction := amaxmsig.NewPropose(eos.AccountName(proposer), eos.Name(proposalName), requested, rawTx)

	tx := eos.NewTransaction([]*eos.Action{proposeAction}, txOpts)

	return tx, nil
}

// PushProposeApproveTransaction 发送通过提案交易
func PushProposeApproveTransaction(ctx context.Context, api *eos.API, actor, proposer, proposalName string) (string, error) {
	txOpts := &eos.TxOptions{}
	if err := txOpts.FillFromChain(ctx, api); err != nil {
		log.Logger.Errorf("Failed to fill tx err: %s", err.Error())
		return "", err
	}

	// 设置签名账户
	level := eos.PermissionLevel{
		Actor:      eos.AccountName(actor),
		Permission: eos.PermissionName("active"),
	}

	approveAction := amaxmsig.NewApprove(eos.AccountName(proposer), eos.Name(proposalName), level)

	// 批准交易进行签名
	tx := eos.NewTransaction([]*eos.Action{approveAction}, txOpts)

	txHash, err := SignAndPush(ctx, api, txOpts, tx)
	if err != nil {
		log.Logger.Errorf("propose approve push transaction err: %s", err.Error())
		return "", err
	}

	return txHash, nil
}

// PushProposeExecTransaction 发送执行提案交易
func PushProposeExecTransaction(ctx context.Context, api *eos.API, txOpts *eos.TxOptions, proposer, proposalName string) (string, error) {
	if txOpts == nil {
		txOpts = &eos.TxOptions{}
		if err := txOpts.FillFromChain(ctx, api); err != nil {
			log.Logger.Errorf("Failed to fill tx err: %s", err.Error())
			return "", err
		}
	}

	execAction := amaxmsig.NewExec(eos.AccountName(proposer), eos.Name(proposalName), eos.AccountName(proposer))

	// 批准交易进行签名
	tx := eos.NewTransaction([]*eos.Action{execAction}, txOpts)

	txHash, err := SignAndPush(ctx, api, txOpts, tx)
	if err != nil {
		log.Logger.Errorf("propose exec push transaction err: %s", err.Error())
		return "", err
	}

	return txHash, nil
}
