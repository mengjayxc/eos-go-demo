package amaxmsig

import eos "github.com/armoniax/eos-go"

type ProposalRow struct {
	ProposalName       eos.Name              `json:"proposal_name"`
	RequestedApprovals []eos.PermissionLevel `json:"requested_approvals"`
	ProvidedApprovals  []eos.PermissionLevel `json:"provided_approvals"`
	PackedTransaction  eos.HexBytes          `json:"packed_transaction"`
}

type Approvals2Row struct {
	Version            int          `json:"version"`
	ProposalName       eos.Name     `json:"proposal_name"`
	RequestedApprovals []Approvals  `json:"requested_approvals"`
	ProvidedApprovals  []Approvals  `json:"provided_approvals"`
	PackedTransaction  eos.HexBytes `json:"packed_transaction"`
}

type Approvals struct {
	Level eos.PermissionLevel `json:"level"`
	Time  string              `json:"time"`
}
