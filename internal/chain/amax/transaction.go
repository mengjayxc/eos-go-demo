package amax

import (
	"context"
	"encoding/hex"
	"eos-go-demo/internal/constant"
	"eos-go-demo/internal/core/log"
	"strings"

	"github.com/armoniax/eos-go"
)

func SignAndPush(ctx context.Context, api *eos.API, txOpts *eos.TxOptions, tx *eos.Transaction) (string, error) {
	_, packedTrx, err := SignTransaction(ctx, api, txOpts, tx)
	if err != nil {
		log.Logger.Errorf("sign transaction err: %s, tx: %+v", err.Error(), *tx)
		return "", err
	}

	txHash, err := PushTransaction(ctx, api, packedTrx)
	if err != nil {
		log.Logger.Errorf("push transaction err: %s", err.Error())
		return "", err
	}

	return txHash, nil
}

type KmsRequest struct {
	KeyId string `json:"key_id"`
	Hash  string `json:"hash"` // 交易打包的hash值
}

type KmsResponse struct {
	Signature string `json:"signature"`  // 签名数据SIG_K1_xxxxxxxx形式
	PublicKey string `json:"public_Key"` // key_id对应的地址AMxxxxx
}

func SignTransaction(ctx context.Context, api *eos.API, txOpts *eos.TxOptions, tx *eos.Transaction) (*eos.SignedTransaction, *eos.PackedTransaction, error) {
	var (
		packed *eos.PackedTransaction
		signed *eos.SignedTransaction
		err    error
	)

	//signType := strings.ToLower(config.ConfigManager.Config.Secret.AmaxAccount.SignType)
	signType := constant.SignTypeLocal
	switch signType {
	case constant.SignTypeLocal:
		signed, packed, err = api.SignTransaction(ctx, tx, txOpts.ChainID, eos.CompressionNone)
		if err != nil {
			return nil, nil, err
		}
		break
		//case constant.SignTypeKms:
		//	stx := eos.NewSignedTransaction(tx)
		//	txdata, cfd, err := stx.PackedTransactionAndCFD()
		//	if err != nil {
		//		return nil, nil, err
		//	}
		//	sigDigest := eos.SigDigest(txOpts.ChainID, txdata, cfd)
		//
		//	data := KmsRequest{
		//		//KeyId: config.ConfigManager.Config.Secret.AmaxAccount.KmsKeyId,
		//		KeyId: "",
		//		Hash:  hex.EncodeToString(sigDigest),
		//	}
		//	r, err := g.Client().ContentJson().Post(ctx, config.ConfigManager.Config.Secret.AmaxAccount.KmsApiUrl, data)
		//	if err != nil {
		//		return nil, nil, err
		//	}
		//	defer r.Close()
		//
		//	var resp KmsResponse
		//	err = helper.GfHttpRespToStruct(r, &resp)
		//	if err != nil {
		//		return nil, nil, err
		//	}
		//
		//	if resp.Signature == "" {
		//		return nil, nil, errors.New("kms signature is null")
		//	}
		//
		//	sig := ecc.MustNewAMASignature(resp.Signature)
		//	stx.Signatures = append(stx.Signatures, sig)
		//	packed, err = stx.Pack(eos.CompressionNone)
		//	if err != nil {
		//		return nil, nil, err
		//	}
		//	break
	}

	return signed, packed, nil
}

func PushTransaction(ctx context.Context, api *eos.API, packedTrx *eos.PackedTransaction) (string, error) {
	response, err := api.PushTransaction(ctx, packedTrx)
	if err != nil {
		return "", err
	}

	txHash := strings.ToUpper(hex.EncodeToString(response.Processed.ID))

	return txHash, nil
}
