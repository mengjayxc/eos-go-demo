package amax

import (
	"context"
	"eos-go-demo/internal/core/log"
	"eos-go-demo/internal/helper"
	"fmt"

	"github.com/armoniax/eos-go"
)

// AmaxPushTransaction amax提交交易
func AmaxPushTransaction(client *eos.API, ctx context.Context, sourceAddress, toAddress, tokenAddress, coinSymbol, memo string, amount uint64, coinDecimal uint8) (string, error) {
	// FIXME: 测试， 开启API DEBUG
	client.Debug = true

	txOpts := &eos.TxOptions{}
	if err := txOpts.FillFromChain(ctx, client); err != nil {
		log.Logger.Errorf("Failed to fill tx err: %s", err.Error())
		return "", err
	}

	tx, err := NewTransaction(client, ctx, txOpts, sourceAddress, toAddress, tokenAddress, coinSymbol, memo, amount, coinDecimal)
	if err != nil {
		log.Logger.Errorf("amax NewTransaction err: %s", err.Error())
		return "", err
	}

	txHash, err := SignAndPush(ctx, client, txOpts, tx)
	if err != nil {
		log.Logger.Errorf("amax transfer push transaction err: %s", err.Error())
		return "", err
	}

	return txHash, nil
}

// NewTransaction 生成新交易体
func NewTransaction(client *eos.API, ctx context.Context, txOpts *eos.TxOptions, sourceAddress, toAddress, tokenAddress, coinSymbol, memo string, amount uint64, coinDecimal uint8) (*eos.Transaction, error) {
	log.Logger.Infof("new transaction, from:%v, to:%v, contract:%v, symbol:%v, amount:%v, decimals:%v, memo:%v",
		sourceAddress, toAddress, tokenAddress, coinSymbol, amount, coinDecimal, memo)

	from := eos.AccountName(sourceAddress)
	to := eos.AccountName(toAddress)
	contract := eos.AccountName(tokenAddress)

	symbol := eos.Symbol{
		Precision: coinDecimal,
		Symbol:    coinSymbol,
	}

	amounts := fmt.Sprintf("%v %s", helper.ToPowDiv(amount, int64(coinDecimal)).String(), coinSymbol)

	quantity, err := eos.NewFixedSymbolAssetFromString(symbol, amounts)
	if err != nil {
		log.Logger.Errorf("Error symbol asset err: %s, symbol: %s, amount: %s", err.Error(), symbol, amounts)
		return nil, err
	}

	if txOpts == nil {
		txOpts = &eos.TxOptions{}
	}
	if err = txOpts.FillFromChain(ctx, client); err != nil {
		log.Logger.Errorf("Failed to fill tx err: %s", err.Error())
		return nil, err
	}

	tx := eos.NewTransaction([]*eos.Action{NewAmaxTransfer(from, to, contract, quantity, memo)}, txOpts)

	return tx, nil
}

func RetryAmaxPushTrx(client *eos.API, ctx context.Context, num int, sourceAddress, toAddress, tokenAddress, coin, memo string, amount uint64, coinDecimal uint8) (string, error) {
	var txHash string
	var err error
	if num >= 1 {
		for i := 0; i < num; i++ {
			txHash, err = AmaxPushTransaction(client, ctx, sourceAddress, toAddress, tokenAddress, coin, memo, amount, coinDecimal)
			if err == nil && txHash != "" {
				break
			}
		}
	}

	return txHash, err
}
