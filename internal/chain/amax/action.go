package amax

import (
	"encoding/json"
	"github.com/armoniax/eos-go"
)

func NewAmaxTransfer(from, to, account eos.AccountName, quantity eos.Asset, memo string) *eos.Action {
	action := &eos.Action{
		Account: account,
		Name:    eos.ActN("transfer"),
		Authorization: []eos.PermissionLevel{
			{Actor: from, Permission: eos.PN("active")},
		},
	}

	transfer := Transfer{
		From:     from,
		To:       to,
		Quantity: quantity,
		Memo:     memo,
	}

	trx, _ := json.Marshal(transfer)

	action.ActionData = eos.NewActionDataFromHexData(trx)
	action.ActionData.Data = transfer

	return action
}

// Transfer represents the `transfer` struct on `eosio.token` contract.
type Transfer struct {
	From     eos.AccountName `json:"from"`
	To       eos.AccountName `json:"to"`
	Quantity eos.Asset       `json:"quantity"`
	Memo     string          `json:"memo"`
}
