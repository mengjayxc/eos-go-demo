package helper

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"io"
	"math/big"
	"net/url"
	"sort"
	"strings"

	"github.com/gogf/gf/v2/net/gclient"
	"github.com/gogf/gf/v2/util/gconv"

	"github.com/shopspring/decimal"
)

// GetToBlock 获取结束区块
// blockNumber 当前最新网络区块
// fromBlock 本地记录区块
func GetToBlock(blockNumber int64, fromBlock int64) int64 {
	difference := blockNumber - fromBlock
	toBlock := blockNumber

	if difference > 500 {
		toBlock = fromBlock + 500
	} else if difference > 100 {
		toBlock = fromBlock + 100
	} else if difference > 30 {
		toBlock = fromBlock + 30
	} else {
		//toBlock = fromBlock + 1
		toBlock = blockNumber
	}

	return toBlock
}

func Remove0x(h string) string {
	trimmed := strings.TrimPrefix(h, "0x")
	if len(trimmed)%2 == 1 {
		trimmed = "0" + trimmed
	}

	return trimmed
}

func Add0x(h string) string {
	if strings.HasPrefix(h, "0x") {
		return h
	}

	return "0x" + h
}

// Convert0xTo41 0x前缀变为41前缀,不是0x且没有41前缀自动追加41
func Convert0xTo41(h string) string {
	if strings.HasPrefix(h, "0x") {
		return "41" + h[2:]
	}
	if !strings.HasPrefix(h, "41") {
		return "41" + h
	}
	return h
}

// Convert41To0x 41前缀变为0x前缀,不是41且没有0x前缀自动追加0x
func Convert41To0x(h string) string {
	if strings.HasPrefix(h, "41") {
		return "0x" + h[2:]
	}
	if !strings.HasPrefix(h, "0x") {
		return "0x" + h
	}
	return h
}

func HexToBigInt(hex string) *big.Int {
	if hex == "0" {
		return big.NewInt(0)
	}
	n := new(big.Int)
	n, _ = n.SetString(hex[2:], 16)

	return n
}

func InArrayString(needle string, haystack []string) bool {
	length := len(haystack)
	for i := 0; i < length; i++ {
		if haystack[i] == needle {
			return true
		}
	}

	return false
}

func InArray(needle interface{}, haystack []interface{}) bool {
	length := len(haystack)
	for i := 0; i < length; i++ {
		if haystack[i] == needle {
			return true
		}
	}

	return false
}

func ToPowDiv(iamount interface{}, decimals int64) decimal.Decimal {
	var amount decimal.Decimal
	switch v := iamount.(type) {
	case string:
		amount, _ = decimal.NewFromString(v)
	case float64:
		amount = decimal.NewFromFloat(v)
	case float32:
		amount = decimal.NewFromFloat32(v)
	case int64:
		amount = decimal.NewFromInt(v)
	case uint64:
		amount = decimal.NewFromInt(int64(v))
	case int:
		amount = decimal.NewFromInt(int64(v))
	case uint:
		amount = decimal.NewFromInt(int64(v))
	case int8:
		amount = decimal.NewFromInt(int64(v))
	case uint8:
		amount = decimal.NewFromInt(int64(v))
	case int16:
		amount = decimal.NewFromInt(int64(v))
	case uint16:
		amount = decimal.NewFromInt(int64(v))
	case int32:
		amount = decimal.NewFromInt32(v)
	case uint32:
		amount = decimal.NewFromInt32(int32(v))
	case decimal.Decimal:
		amount = v
	case *decimal.Decimal:
		amount = *v
	case *big.Int:
		amount = decimal.NewFromBigInt(v, 0)
	case big.Int:
		amount = decimal.NewFromBigInt(&v, 0)
	}

	div := decimal.NewFromInt(10).Pow(decimal.NewFromInt(decimals))
	result := amount.DivRound(div, int32(decimals))

	return result
}

func ToPowMul(iamount interface{}, decimals int64) decimal.Decimal {
	var amount decimal.Decimal
	switch v := iamount.(type) {
	case string:
		amount, _ = decimal.NewFromString(v)
	case float64:
		amount = decimal.NewFromFloat(v)
	case float32:
		amount = decimal.NewFromFloat32(v)
	case int64:
		amount = decimal.NewFromInt(v)
	case uint64:
		amount = decimal.NewFromInt(int64(v))
	case int:
		amount = decimal.NewFromInt(int64(v))
	case uint:
		amount = decimal.NewFromInt(int64(v))
	case int8:
		amount = decimal.NewFromInt(int64(v))
	case uint8:
		amount = decimal.NewFromInt(int64(v))
	case int16:
		amount = decimal.NewFromInt(int64(v))
	case uint16:
		amount = decimal.NewFromInt(int64(v))
	case int32:
		amount = decimal.NewFromInt32(v)
	case uint32:
		amount = decimal.NewFromInt32(int32(v))
	case decimal.Decimal:
		amount = v
	case *decimal.Decimal:
		amount = *v
	case *big.Int:
		amount = decimal.NewFromBigInt(v, 0)
	case big.Int:
		amount = decimal.NewFromBigInt(&v, 0)
	}

	mul := decimal.NewFromInt(10).Pow(decimal.NewFromInt(decimals))
	result := amount.Mul(mul)

	return result
}

// RsaEncrypt rsa加密
// src 要加密的数据
// 公钥
func RsaEncrypt(src []byte, pub []byte) ([]byte, error) {
	block, _ := pem.Decode(pub)
	if block == nil {
		return nil, errors.New("public key error")
	}
	pubInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	publicKey := pubInterface.(*rsa.PublicKey)

	result, _ := rsa.EncryptPKCS1v15(rand.Reader, publicKey, src)

	return result, nil
}

// RsaDecrypt rsa加密
// src 要解密的数据
// 私钥
func RsaDecrypt(src []byte, pri []byte) ([]byte, error) {
	block, _ := pem.Decode(pri)
	if block == nil {
		return nil, errors.New("private key error")
	}

	privateKey, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	prikey, ok := privateKey.(*rsa.PrivateKey)
	if !ok {
		return nil, errors.New("private key not supported")
	}

	result, _ := rsa.DecryptPKCS1v15(rand.Reader, prikey, src)

	return result, nil
}

func SortParams(params map[string]string) string {
	keys := make([]string, len(params))
	i := 0
	for k := range params {
		keys[i] = k
		i++
	}
	sort.Strings(keys)
	sorted := make([]string, len(params))
	i = 0
	for _, k := range keys {
		sorted[i] = k + "=" + url.QueryEscape(params[k])
		i++
	}
	return strings.Join(sorted, "&")
}

// GfHttpRespToStruct GoFrame的httpClient组件响应赋值给结构体
func GfHttpRespToStruct(resp *gclient.Response, result interface{}) error {
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if len(body) == 0 {
		return errors.New("body is null")
	}

	body = bytes.TrimPrefix(body, []byte("\xef\xbb\xbf"))
	err = gconv.Scan(body, &result)
	//err = json.Unmarshal(body, &result)
	if err != nil {
		return err
	}

	return nil
}

func SplitCoin(coin string) (string, string) {
	// 无分隔符，仅为主币
	if !strings.Contains(coin, "_") {
		return coin, coin
	}

	coins := strings.Split(coin, "_")

	return coins[0], coins[1]
}

// ToEOSName 数字转为eos名称的格式
func ToEOSName(n int64) string {
	eosRules := ".12345abcdefghijklmnopqrstuvwxyz"
	s := ""
	for n > 0 {
		s = string(eosRules[n%32]) + s
		n /= 32
	}
	if len(s) > 12 {
		s = s[:12]
	}

	return s
}

// FromEOSName eos名称转数字
func FromEOSName(digits string) int64 {
	eosRules := ".12345abcdefghijklmnopqrstuvwxyz"
	n := int64(0)
	for _, c := range digits {
		n = n*32 + int64(strings.IndexByte(eosRules, byte(c)))
	}

	return n
}
