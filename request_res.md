eos 错误码：

https://blog.csdn.net/agecntao/article/details/81219569

get_actions:

```json
{
  "last_irreversible_block": 135631145,
  "actions": [{
    "account_action_seq": 1471231,
    "global_action_seq": 151769097,
    "block_num": 135451269,
    "block_time": "2022-04-15T08:46:55.000",
    "action_trace": {
      "action_ordinal": 13,
      "creator_action_ordinal": 3,
      "receipt": {
        "receiver": "eosio.token",
        "global_sequence": 151769097,
        "recv_sequence": 1471231,
        "auth_sequence": [{
          "account": "eosisverygo1",
          "sequence": 520
        }]
      },
      "receiver": "eosio.token",
      "act": {
        "account": "eosio.token",
        "name": "transfer",
        "authorization": [{
          "actor": "eosisverygo1",
          "permission": "active"
        }],
        "data": {
          "from": "eosisverygo1",
          "to": "eosio.stake",
          "amount": 0.2,
          "symbol": "EOS",
          "memo": "stake bandwidth",
          "quantity": "0.2000 EOS"
        },
        "hex_data": "7b2266726f6d223a22656f73697376657279676f31222c22746f223a22656f73696f2e7374616b65222c22616d6f756e74223a302e322c2273796d626f6c223a22454f53222c226d656d6f223a227374616b652062616e647769647468222c227175616e74697479223a22302e3230303020454f53227d"
      },
      "trx_id": "37cd2e40b9b4d298ce49adb74653ee121d87ccd499c101b40e6612d709425534",
      "block_num": 135451269,
      "block_time": "2022-04-15T08:46:55.000"
    }
  }]
}
```

signedTx:

```json
{
	"expiration": "2022-04-18T07:45:24",
	"ref_block_num": 40451,
	"ref_block_prefix": 2879039367,
	"max_net_usage_words": 0,
	"max_cpu_usage_ms": 0,
	"delay_sec": 0,
	"context_free_actions": [],
	"actions": [{
		"account": "eosio.token",
		"name": "delegatebw",
		"authorization": [{
			"actor": "eosisverygo1",
			"permission": "active"
		}],
		"data": "1028f3576dec30551028f3576dec305500000000000000000403454f53000000000000000050c30000000000000403454f53000000000000000000"
	}],
	"transaction_extensions": [],
	"signatures": [
		"SIG_K1_JxTAHrWhPXhVoGB3sYybZ1SDmW7Cq1KXCM3mGjqtbiDN3TfmGsTG8fioq2bgHUyMGcjR5uWz2JSsqXVmnyf1Q18BqsY8aM"
	],
	"context_free_data": []
}
```

pushTransaction:

panic: push transaction: Internal Service Error: Expired Transaction: expired transaction a656866ddf473851102f14017f20cf8fda9bf4aff3ca9d7d90574314fd964ba8, expiration 2022-04-18T07:45:24.000, block time 2022-04-18T07:45:36.000

panic: push transaction: Internal Service Error: eosio_assert_code assertion failure: assertion failure with error code: 8000000000000000000: pending console output:


HTTP / 1.1 500 Internal Server Error
Access - Control - Allow - Headers: Origin, X - Requested - With, Content - Type, Accept
Access - Control - Allow - Origin: *
Connection: close
Content - Length: 408
Content - Type: application / json
Server: WebSocket++/0.7.0


代码条件验证失败

```json
{
  "code": 500,
  "message": "Internal Service Error",
  "error": {
    "code": 3050004,
    "name": "eosio_assert_code_exception",
    "what": "eosio_assert_code assertion failure",
    "details": [{
      "message": "assertion failure with error code: 8000000000000000000",
      "file": "cf_system.cpp",
      "line_number": 41,
      "method": "eosio_assert_code"
    }, {
      "message": "pending console output: ",
      "file": "apply_context.cpp",
      "line_number": 143,
      "method": "exec_one"
    }]
  }
}
```

错误查询：

https://community.eosstudio.io/-117/assertion-failure-with-error-code-8000000000000000000



----------------------
划转成功了：

quantity: 1.0000 JUNGLE

signedTx:

```json
{
	"expiration": "2022-04-18T08:56:07",
	"ref_block_num": 48942,
	"ref_block_prefix": 255392266,
	"max_net_usage_words": 0,
	"max_cpu_usage_ms": 0,
	"delay_sec": 0,
	"context_free_actions": [],
	"actions": [{
		"account": "eosio.token",
		"name": "transfer",
		"authorization": [{
			"actor": "eosisverygo1",
			"permission": "active"
		}],
		"data": "1028f3576dec30552028f3576dec30551027000000000000044a554e474c450000"
	}],
	"transaction_extensions": [],
	"signatures": [
		"SIG_K1_KfCJ2hh38FZeidpkNUafEMPD6WqcvSBVJRh28qjVfRpXPzn419pVCoiqkty4kWiTkbotZ5BA1BExGGnisT3712oKTcd1ZT"
	],
	"context_free_data": []
}
```

Transaction [7ee64a282f6e69294efe36fd33e3898af6176c4a4b05c7f025ebf66226801e1f] submitted to the network succesfully.

actions:

```json
{
	"last_irreversible_block": 135970867,
	"actions": [{
		"account_action_seq": 1472221,
		"global_action_seq": 151855553,
		"block_num": 135970609,
		"block_time": "2022-04-18T08:55:38.500",
		"action_trace": {
			"action_ordinal": 1,
			"creator_action_ordinal": 0,
			"receipt": {
				"receiver": "eosio.token",
				"global_sequence": 151855553,
				"recv_sequence": 1472221,
				"auth_sequence": [{
					"account": "eosisverygo1",
					"sequence": 523
				}]
			},
			"receiver": "eosio.token",
			"act": {
				"account": "eosio.token",
				"name": "transfer",
				"authorization": [{
					"actor": "eosisverygo1",
					"permission": "active"
				}],
				"data": {
					"from": "eosisverygo1",
					"to": "eosisverygo2",
					"amount": 1,
					"symbol": "JUNGLE",
					"memo": "",
					"quantity": "1.0000 JUNGLE"
				},
				"hex_data": "7b2266726f6d223a22656f73697376657279676f31222c22746f223a22656f73697376657279676f32222c22616d6f756e74223a312c2273796d626f6c223a224a554e474c45222c226d656d6f223a22222c227175616e74697479223a22312e30303030204a554e474c45227d"
			},
			"trx_id": "7ee64a282f6e69294efe36fd33e3898af6176c4a4b05c7f025ebf66226801e1f",
			"block_num": 135970609,
			"block_time": "2022-04-18T08:55:38.500"
		}
	}]
}
```

stake:

signedTx:

```json
{
  "expiration": "2022-04-18T09:42:07",
  "ref_block_num": 54463,
  "ref_block_prefix": 2073023879,
  "max_net_usage_words": 0,
  "max_cpu_usage_ms": 0,
  "delay_sec": 0,
  "context_free_actions": [],
  "actions": [
    {
      "account": "eosio",
      "name": "delegatebw",
      "authorization": [
        {
          "actor": "eosisverygo1",
          "permission": "active"
        }
      ],
      "data": "1028f3576dec30551028f3576dec3055000000000000000004454f5300000000102700000000000004454f530000000000"
    }
  ],
  "transaction_extensions": [],
  "signatures": [
    "SIG_K1_KBC4wDCj4N272pidvBW2i2AVwfem3Kw6PiGXCmHDPMcwXVNY7bEKWwqS4zB6Pa2bsBimJqK4TL9nktQf1yJhoLY2GgsCPq"
  ],
  "context_free_data": []
}
```

``
res: &{ db7f3a41b27af87e557c2c659a1b5d9b598d78aa01dc462f225d6094517c883c { db7f3a41b27af87e557c2c659a1b5d9b598d78aa01dc462f225d6094517c883c [{eosio  []}] []}  0}
Transaction [db7f3a41b27af87e557c2c659a1b5d9b598d78aa01dc462f225d6094517c883c] submitted to the network succesfully.
``

success history actions:

```json
{
	"last_irreversible_block": 135980143,
	"actions": [{
		"account_action_seq": 1472237,
		"global_action_seq": 151856949,
		"block_num": 135978918,
		"block_time": "2022-04-18T10:04:53.000",
		"action_trace": {
			"action_ordinal": 2,
			"creator_action_ordinal": 1,
			"receipt": {
				"receiver": "eosio.token",
				"global_sequence": 151856949,
				"recv_sequence": 1472237,
				"auth_sequence": [{
					"account": "eosisverygo1",
					"sequence": 534
				}]
			},
			"receiver": "eosio.token",
			"act": {
				"account": "eosio.token",
				"name": "transfer",
				"authorization": [{
					"actor": "eosisverygo1",
					"permission": "active"
				}],
				"data": {
					"from": "eosisverygo1",
					"to": "eosio.stake",
					"amount": 3,
					"symbol": "EOS",
					"memo": "stake bandwidth",
					"quantity": "3.0000 EOS"
				},
				"hex_data": "7b2266726f6d223a22656f73697376657279676f31222c22746f223a22656f73696f2e7374616b65222c22616d6f756e74223a332c2273796d626f6c223a22454f53222c226d656d6f223a227374616b652062616e647769647468222c227175616e74697479223a22332e3030303020454f53227d"
			},
			"trx_id": "31ab6b1ca952dd5054794b2198a7eaf01ade78986bd4c453d46e715896dd1038",
			"block_num": 135978918,
			"block_time": "2022-04-18T10:04:53.000"
		}
	}, {
		"account_action_seq": 118200355,
		"global_action_seq": 151856948,
		"block_num": 135978918,
		"block_time": "2022-04-18T10:04:53.000",
		"action_trace": {
			"action_ordinal": 1,
			"creator_action_ordinal": 0,
			"receipt": {
				"receiver": "eosio",
				"global_sequence": 151856948,
				"recv_sequence": 118200355,
				"auth_sequence": [{
					"account": "eosisverygo1",
					"sequence": 533
				}]
			},
			"receiver": "eosio",
			"act": {
				"account": "eosio",
				"name": "delegatebw",
				"authorization": [{
					"actor": "eosisverygo1",
					"permission": "active"
				}],
				"data": {
					"amount": 3,
					"stake_cpu_quantity": 2,
					"stake_net_quantity": 1,
					"from": "eosisverygo1",
					"receiver": "eosisverygo1",
					"transfer": false
				},
				"hex_data": "7b22616d6f756e74223a332c227374616b655f6370755f7175616e74697479223a322c227374616b655f6e65745f7175616e74697479223a312c2266726f6d223a22656f73697376657279676f31222c227265636569766572223a22656f73697376657279676f31222c227472616e73666572223a66616c73657d"
			},
			"trx_id": "31ab6b1ca952dd5054794b2198a7eaf01ade78986bd4c453d46e715896dd1038",
			"block_num": 135978918,
			"block_time": "2022-04-18T10:04:53.000"
		}
	}, {
		"account_action_seq": 1472233,
		"global_action_seq": 151856483,
		"block_num": 135976131,
		"block_time": "2022-04-18T09:41:39.500",
		"action_trace": {
			"action_ordinal": 2,
			"creator_action_ordinal": 1,
			"receipt": {
				"receiver": "eosio.token",
				"global_sequence": 151856483,
				"recv_sequence": 1472233,
				"auth_sequence": [{
					"account": "eosisverygo1",
					"sequence": 530
				}]
			},
			"receiver": "eosio.token",
			"act": {
				"account": "eosio.token",
				"name": "transfer",
				"authorization": [{
					"actor": "eosisverygo1",
					"permission": "active"
				}],
				"data": {
					"from": "eosisverygo1",
					"to": "eosio.stake",
					"amount": 1,
					"symbol": "EOS",
					"memo": "stake bandwidth",
					"quantity": "1.0000 EOS"
				},
				"hex_data": "7b2266726f6d223a22656f73697376657279676f31222c22746f223a22656f73696f2e7374616b65222c22616d6f756e74223a312c2273796d626f6c223a22454f53222c226d656d6f223a227374616b652062616e647769647468222c227175616e74697479223a22312e3030303020454f53227d"
			},
			"trx_id": "db7f3a41b27af87e557c2c659a1b5d9b598d78aa01dc462f225d6094517c883c",
			"block_num": 135976131,
			"block_time": "2022-04-18T09:41:39.500"
		}
	}, {
		"account_action_seq": 118200351,
		"global_action_seq": 151856482,
		"block_num": 135976131,
		"block_time": "2022-04-18T09:41:39.500",
		"action_trace": {
			"action_ordinal": 1,
			"creator_action_ordinal": 0,
			"receipt": {
				"receiver": "eosio",
				"global_sequence": 151856482,
				"recv_sequence": 118200351,
				"auth_sequence": [{
					"account": "eosisverygo1",
					"sequence": 529
				}]
			},
			"receiver": "eosio",
			"act": {
				"account": "eosio",
				"name": "delegatebw",
				"authorization": [{
					"actor": "eosisverygo1",
					"permission": "active"
				}],
				"data": {
					"amount": 1,
					"stake_cpu_quantity": 1,
					"stake_net_quantity": 0,
					"from": "eosisverygo1",
					"receiver": "eosisverygo1",
					"transfer": false
				},
				"hex_data": "7b22616d6f756e74223a312c227374616b655f6370755f7175616e74697479223a312c227374616b655f6e65745f7175616e74697479223a302c2266726f6d223a22656f73697376657279676f31222c227265636569766572223a22656f73697376657279676f31222c227472616e73666572223a66616c73657d"
			},
			"trx_id": "db7f3a41b27af87e557c2c659a1b5d9b598d78aa01dc462f225d6094517c883c",
			"block_num": 135976131,
			"block_time": "2022-04-18T09:41:39.500"
		}
	}, {
		"account_action_seq": 1472225,
		"global_action_seq": 151855739,
		"block_num": 135971650,
		"block_time": "2022-04-18T09:04:19.000",
		"action_trace": {
			"action_ordinal": 1,
			"creator_action_ordinal": 0,
			"receipt": {
				"receiver": "eosio.token",
				"global_sequence": 151855739,
				"recv_sequence": 1472225,
				"auth_sequence": [{
					"account": "eosisverygo1",
					"sequence": 526
				}]
			},
			"receiver": "eosio.token",
			"act": {
				"account": "eosio.token",
				"name": "transfer",
				"authorization": [{
					"actor": "eosisverygo1",
					"permission": "active"
				}],
				"data": {
					"from": "eosisverygo1",
					"to": "eosisverygo2",
					"amount": 1,
					"symbol": "JUNGLE",
					"memo": "",
					"quantity": "1.0000 JUNGLE"
				},
				"hex_data": "7b2266726f6d223a22656f73697376657279676f31222c22746f223a22656f73697376657279676f32222c22616d6f756e74223a312c2273796d626f6c223a224a554e474c45222c226d656d6f223a22222c227175616e74697479223a22312e30303030204a554e474c45227d"
			},
			"trx_id": "95bcd89b2b7f4df5377c5bec501249f75032ef6603279ea12095750ab3c13e8c",
			"block_num": 135971650,
			"block_time": "2022-04-18T09:04:19.000"
		}
	}, {
		"account_action_seq": 1472221,
		"global_action_seq": 151855553,
		"block_num": 135970609,
		"block_time": "2022-04-18T08:55:38.500",
		"action_trace": {
			"action_ordinal": 1,
			"creator_action_ordinal": 0,
			"receipt": {
				"receiver": "eosio.token",
				"global_sequence": 151855553,
				"recv_sequence": 1472221,
				"auth_sequence": [{
					"account": "eosisverygo1",
					"sequence": 523
				}]
			},
			"receiver": "eosio.token",
			"act": {
				"account": "eosio.token",
				"name": "transfer",
				"authorization": [{
					"actor": "eosisverygo1",
					"permission": "active"
				}],
				"data": {
					"from": "eosisverygo1",
					"to": "eosisverygo2",
					"amount": 1,
					"symbol": "JUNGLE",
					"memo": "",
					"quantity": "1.0000 JUNGLE"
				},
				"hex_data": "7b2266726f6d223a22656f73697376657279676f31222c22746f223a22656f73697376657279676f32222c22616d6f756e74223a312c2273796d626f6c223a224a554e474c45222c226d656d6f223a22222c227175616e74697479223a22312e30303030204a554e474c45227d"
			},
			"trx_id": "7ee64a282f6e69294efe36fd33e3898af6176c4a4b05c7f025ebf66226801e1f",
			"block_num": 135970609,
			"block_time": "2022-04-18T08:55:38.500"
		}
	}]
}
```

